﻿using System;
using System.Collections.Generic;
using Data.Model;
using Data.Repository.Interfaces;
using Data.DataFactory;
using System.Linq;
using Data.Extensions;

namespace Data.Repository
{
    /// <summary>
    /// The repository for stations data
    /// </summary>
    public sealed class StationRepository : IRepository
    {
        private StationDataSource _dataSource;
        private bool _success;
        private string _message;

        public StationRepository()
        {
            _success = true;
            _message = "";

            try
            {
                //get the text data file path
                var MVCpath = AppDomain.CurrentDomain.BaseDirectory;
                //Just to perform the tests (unnecessary code in a real world scenario)----------
                int ajust = 12;

                if (AppDomain.CurrentDomain.GetAssemblies().Any(a => a.FullName.StartsWith("TrainTicketTests")))
                {
                    ajust = 26;
                }
                var libraryPath = MVCpath.Substring(0, MVCpath.Length - ajust);
                var file = libraryPath + "data\\datafiles\\stations.txt";
                //-------------------------------------------------------------------------------
                //Inversion of control for the data source
                _dataSource = new StationDataSource(new TextDataSource(file,true));
            }
            catch(Exception ex)
            {
                _success = false;
                _message = ex.Message;
            }            
        }

        /// <summary>
        /// Tells whether an operations was performed successfully
        /// </summary>
        public bool Success
        {
            get { return _success; }
        }

        /// <summary>
        /// A message returned when a operation was not performed successfully
        /// </summary>
        public string Message
        {
            get { return _message; }
        }

        /// <summary>
        /// Get all stations
        /// </summary>
        public IList<Station> GetAll()
        {
            _success = true;
            _message = "";

            try
            {
                return _dataSource.Data;
            }
            catch(Exception ex)
            {
                _success = false;
                _message = ex.Message;
                return null;
            }
        }

        /// <summary>
        /// Returns a list of stations that match the searched word
        /// </summary>
        /// <param name="search">Word being sought</param>
        public TypeAheadResult GetTypeAhead(string search)
        {
            if (search == null) search = "";
            var stations = _dataSource.Data.Where(x => x.Name.ToUpper().StartsWith(search.ToUpper())).ToList();
            var nextChars = NextChars(stations, search);
            var ret = new TypeAheadResult(stations, nextChars);

            return ret;
        }

        /// <summary>
        /// Returns a list of next chars based on the station list returned by GetTypeAhead
        /// </summary>
        /// <param name="stations">All stations that match a given word</param>
        /// <param name="searched">the word being sought</param>
        private List<char> NextChars(List<Station> stations, string searched)
        {
            var ret = new List<char>();
            if (searched.Trim() == "") return ret;

            foreach (var station in stations)
            {
                var next = station.Name.NextChar(searched);
                if (!ret.Contains(next)) ret.Add(next);
            }

            return ret;
        }
    }
}
