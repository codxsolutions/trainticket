﻿using Data.Model;
using System.Collections.Generic;

namespace Data.Repository.Interfaces
{
    interface IRepository
    {
        IList<Station> GetAll();
        TypeAheadResult GetTypeAhead(string search);
        bool Success { get; }
        string Message { get; }
    }
}
