﻿using System;

namespace Data.Extensions
{
    /// <summary>
    /// Extension for stations
    /// </summary>
    public static class StationEx
    {
        /// <summary>
        /// Gets the next character of one string based on a givem word
        /// </summary>
        /// <param name="value">the string value</param>
        /// <param name="searched">the word sought</param>
        public static char NextChar(this string value, string searched)
        {
            char ret = char.Parse(value.Substring(searched.Length, 1));
            return Char.ToUpper(ret);
        }
    }
}
