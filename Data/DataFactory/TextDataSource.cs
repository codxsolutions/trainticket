﻿using Data.DataFactory.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

/// <summary>
/// Mock the data access
/// </summary>
namespace Data.DataFactory
{
    /// <summary>
    /// Data source based on text files
    /// </summary>
    public sealed class TextDataSource : IDataSource
    {
        private string _file;
        private List<string> _data;

        /// <summary>
        /// Initializes the data source
        /// </summary>
        /// <param name="file_path">Text data file path</param>
        /// <param name="open">Tells whether open da data file</param>
        public TextDataSource(string file_path, bool open = false)
        {
            _file = file_path;
            _data = new List<string>();

            if (open) Open();
        }

        /// <summary>
        /// Returns the records
        /// </summary>
        public List<string> Data
        {
            get { return _data; }
        }

        /// <summary>
        /// Open the data source
        /// </summary>
        public void Open()
        {
            if (_file.Trim() == "") throw new Exception("file path not provided");
            if (!File.Exists(_file)) throw new Exception($"File {_file} not found.");

            GetFileContent(_file);
        }

        /// <summary>
        /// Loads the data file content
        /// </summary>
        /// <param name="path"></param>
        private void GetFileContent(string path)
        {
            StringBuilder sb = new StringBuilder();
            using (StreamReader sr = File.OpenText(path))
            {
                string line = "";
                while ((line = sr.ReadLine()) != null)
                {
                    _data.Add(line);
                }
            }
        }
    }
}
