﻿using Data.DataFactory.Interfaces;
using Data.Model;
using System.Collections.Generic;

namespace Data.DataFactory
{
    /// <summary>
    /// Data source for reading the stations from a given data source
    /// </summary>
    public sealed class StationDataSource
    {
        private IDataSource _dataSource;

        /// <summary>
        /// Initializes the data source
        /// </summary>
        /// <param name="dataSource">Provide a data source</param>
        public StationDataSource(IDataSource dataSource)
        {
            _dataSource = dataSource;
        }

        /// <summary>
        /// Returns the records
        /// </summary>
        public IList<Station> Data {
            get
            {
                return GetList();
            }
        }

        private IList<Station> GetList()
        {
            var dataList = new List<Station>();

            foreach(var item in _dataSource.Data)
            {
                var line = item.Split(',');
                dataList.Add(new Station(int.Parse(line[0]), line[1]));
            }

            return dataList;
        }
    }
}
