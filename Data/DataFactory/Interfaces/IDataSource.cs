﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DataFactory.Interfaces
{
    public interface IDataSource
    {
        void Open();
        List<string> Data { get; }
    }
}
