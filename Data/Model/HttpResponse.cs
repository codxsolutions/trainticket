﻿namespace Data.Model
{
    public class HttpResponse
    {
        private dynamic _data;
        private bool _success;
        private string _message;

        public HttpResponse(bool success, string message, dynamic data)
        {
            _success = success;
            _message = message;
            _data = data;
        }

        public dynamic Data
        {
            get { return _data; }
        }

        public bool Success
        {
            get { return _success; }
        }

        public string Message
        {
            get { return _message; }
        }
    }
}
