﻿using System;
using System.Collections.Generic;

namespace Data.Model
{
    public class TypeAheadResult
    {
        private List<Station> _stations;
        private List<char> _nextChars;

        public TypeAheadResult(List<Station> stations, List<char> nextChars)
        {
            _stations = stations;
            _nextChars = nextChars;
        }

        public IList<Station> Stations {
            get { return _stations; }
        }
        public IList<char> NextChars {
            get { return _nextChars; }
        }
    }
}
