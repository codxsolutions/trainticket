﻿namespace Data.Model
{
    public class Station
    {
        private int _id;
        private string _name;

        public Station(int id, string name)
        {
            _id = id;
            _name = name;
        }

        public int Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }
    }
}
