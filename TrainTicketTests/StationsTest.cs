﻿using Data.Model;
using Data.Repository;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace TrainTicketTests
{
    [TestClass]
    public class StationsTest
    {
        [TestMethod]
        public void Stations_List_All()
        {
            var repo = new StationRepository();
            var result = repo.GetAll();
            Assert.AreEqual(result.Count, 34);
        }

        [TestMethod]
        public void Stations_Found_Quantity()
        {
            var repo = new StationRepository();
            TypeAheadResult result = repo.GetTypeAhead("Dart");
            Assert.AreEqual(result.Stations.Count,2);
        }

        [TestMethod]
        public void Stations_Found_Correct_Ones()
        {
            var repo = new StationRepository();
            TypeAheadResult result = repo.GetTypeAhead("Dart");
            Assert.AreEqual(result.Stations[0].Name, "Dartford");
            Assert.AreEqual(result.Stations[1].Name, "Dartmouth");
        }

        [TestMethod]
        public void Stations_Found_Correct_Next_Chars()
        {
            var repo = new StationRepository();
            TypeAheadResult result = repo.GetTypeAhead("Dart");
            Assert.AreEqual(result.NextChars[0], 'F');
            Assert.AreEqual(result.NextChars[1], 'M');
        }
    }
}
