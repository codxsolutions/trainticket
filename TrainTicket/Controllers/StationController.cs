﻿using Data.Model;
using Data.Repository;
using System.Collections.Generic;
using System.Web.Http;

namespace TrainTicket.Controllers
{
    [RoutePrefix("api/station")]
    public class StationController : ApiController
    {
        [AllowAnonymous]
        [Route("TypeAhead")]
        public HttpResponse GetTypeAhead(string searched)
        {
            var repo = new StationRepository();

            if (!repo.Success) return new HttpResponse(false, repo.Message, null);

            TypeAheadResult result = repo.GetTypeAhead(searched);

            return new HttpResponse(true, "", result);
        }

        [AllowAnonymous]
        [Route("ListAll")]
        public HttpResponse GetListAll()
        {
            var repo = new StationRepository();

            if (!repo.Success) return new HttpResponse(false, repo.Message, null);

            var result = repo.GetAll();

            return new HttpResponse(true, "", result);
        }
    }
}
