﻿var app = angular.module("mainApp", []);
app.factory('HttpService', HttpService);

function HttpService($http) {
    var service = {};
    service.executeRequest = executeRequest;
    return service;

    function executeRequest(url, method, data, contentType, token, success, error) {
        var auth = null;
        method = method || 'GET';
        data = data || null;

        if (token) $http.defaults.headers.common['Authorization'] = 'Bearer ' + token;

        if (contentType) $http.defaults.headers.common['Content-Type'] = contentType;

        return $http({
            method: method,
            url: url,
            data: data
        }).then(function (response) {
            $('#waitdiv').remove();
            success(response);
        }, function (response) {
            error(response);
        });
    }    
}