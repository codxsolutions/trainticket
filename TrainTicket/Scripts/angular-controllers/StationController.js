﻿(function () {
    app.controller("StationController", ["$scope", "HttpService", function ($scope, HttpService) {
        $scope.list = [];
        $scope.nextChars = [];
        $scope.typeAhead = typeAhead;
        $scope.selChar = selChar;

        listAll();

        function listAll() {
            var url = "/api/station/ListAll";
            $scope.nextChars = [];
           
            HttpService.executeRequest(url, 'GET', null, 'application/json', "", successCallBack, errorCallback);

            function successCallBack(response) {
                if (response.data.Success) {
                    $scope.list = response.data.Data;
                } else {
                    alert(response.data.Message)
                }
            }
            function errorCallback(response) {
               
                alert(response.toString());
            }
        }

        function typeAhead() {
            var url = "/api/station/TypeAhead?searched=" + $scope.searchText;
            $scope.nextChars = [];

            HttpService.executeRequest(url, 'GET', null, 'application/json', "", successCallBack, errorCallback);

            function successCallBack(response) {
                if (response.data.Success) {
                    $scope.list = response.data.Data.Stations;
                    $scope.nextChars = response.data.Data.NextChars;
                } else {
                    alert(response.data.Message)
                }
            }
            function errorCallback(response) {

                alert(response.toString());
            }
        }

        function selChar(char) {
            return $scope.nextChars.indexOf(char) >= 0;
        }
    }]);
})();